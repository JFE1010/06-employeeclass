public class Employee {

    private int idNumber;

    public int getIdNumber() {
        return idNumber;
    }

    public Employee(int idNumber) {
        this.idNumber = idNumber;
    }

    public boolean hasAdministratorRights() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        Employee emp = (Employee) o;
        return idNumber == emp.idNumber;
    }
}
