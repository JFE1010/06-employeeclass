public class Manager extends Employee {

    public Manager(int idNumber) {
        super(idNumber);
    }
    @Override
    public boolean hasAdministratorRights() {
        return true;
    }

}
